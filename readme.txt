   ___    _   _   ____     ___    _       _     
  / _ \  | | | | |  _ \   / _ \  | |     | |    
 | | | | | | | | | |_) | | | | | | |     | |    
 | |_| | | |_| | |  __/  | |_| | | |___  | |___ 
  \___/   \___/  |_|      \___/  |_____| |_____|
                                            

Asignatura: TSW

Arquitectura SPA

Nombre aplicación: Oupoll

Autores:
	Brais Santos Negreira
	Jonatan Couto Riadigos
	Sergio Martinéz Viso

Para el correcto funcionamiento de la aplicación:
 -Descomprimir la carpeta, dentro se encontrará:
	-la carpeta TSW_SPA que contendrá el código fuente:
 		-La carpeta TSW_SPA descomprimida tiene que estar en el raiz del host ej. "http://localhost/TSW_SPA/" ( el código fuente dentro de TSW_SPA ) 
 		-si no se sigue el procedimiento anterior, se debe cambiar la ruta que aparece en el archivo /rest/.htaccess
 	
	-el archivo oupoll.sql que contendrá la base de datos, este se importará o copiará las sentencias para ser ejecutadas.

Ususarios de prueba insertados en oupollsql

|    correo      | contraseña |
|----------------|------------|
|dani@gmail.com  | dani	      |
|brais@gmail.com | brais      |
|________________|____________|

  ____    _          __                  _               _   _ 
 |  _ \  (_)  ___   / _|  _ __   _   _  | |_    __ _    | | | |
 | | | | | | / __| | |_  | '__| | | | | | __|  / _` |   | | | |
 | |_| | | | \__ \ |  _| | |    | |_| | | |_  | (_| |   |_| |_|
 |____/  |_| |___/ |_|   |_|     \__,_|  \__|  \__,_|   (_) (_)


!!!--- Solo si falla la importación o no funciona la BD ---!!!

 -Si por algún casual si no funciona la importación correctamente asegurese de los siguientes apartados:

 -Hay que crear la base de datos con el nombre oupoll e importar el fichero oupoll.sql

 -Es necesario crear el usuario tsw con contraseña tsw para conectarse con la base de datos. Esto puede verse en el fichero Functions/BdAdmin.php

 Si el error permanece pongase en contato con nosotros:
 	santosbrais@gmail.com
 	sergiomv14@gmail.com
 	jonycr717@gmail.com
