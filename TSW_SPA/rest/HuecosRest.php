<?php

require_once(__DIR__."/../Models/model_hueco.php");
require_once(__DIR__."/BaseRest.php");
require_once(__DIR__."/../Exceptions/ValidationException.php");
/**
* Class PostRest
*
* It contains operations for creating, retrieving, updating, deleting and
* listing posts, as well as to create comments to posts.
*
* Methods gives responses following Restful standards. Methods of this class
* are intended to be mapped as callbacks using the URIDispatcher class.
*
*/
class HuecosRest extends BaseRest {
	private $HUECO;

	public function __construct() {
		parent::__construct();
		$this->HUECO = new HUECO( '', '', '', '', '' );
	}


	public function getHuecos($link) {
		$currentEmail = parent::authenticateUser()->email;
		
		if ( isset( $link ) ) {
			$this->HUECO->link=$link;
		}

		//recuperamos las encuestas del usuario logeado
		$huecos = $this->HUECO->TODOS_LOS_HUECOS();

		// json_encode Post objects.
		// since Post objects have private fields, the PHP json_encode will not
		// encode them, so we will create an intermediate array using getters and
		// encode it finally
		$huecos_array = array();

		foreach($huecos as $datos) {
			array_push($huecos_array, array(
				"fecha" => $datos['fecha'],
				"hora_inicio" => $datos['hora_inicio'],
				"hora_fin" => $datos['hora_fin'],
				"numero" => $datos['numero'],
				"link" => $datos['link']
			));
		}

		header($_SERVER['SERVER_PROTOCOL'].' 200 Ok');
		header('Content-Type: application/json');
		echo(json_encode($huecos_array));
	}

	public function crearHueco($data) {
		$currentEmail = parent::authenticateUser()->email;

		if ( isset($data->link) && isset($data->fecha) && isset($data->hora_inicio) && isset($data->hora_fin) ) {
			$this->HUECO->link = $data->link;
			$this->HUECO->fecha = $data->fecha;
			$this->HUECO->hora_inicio = $data->hora_inicio;
			$this->HUECO->hora_fin = $data->hora_fin;
		}

		try {
			// validate Post object
			$this->HUECO->checkIsValidForCreate(); // if it fails, ValidationException

			// save the Post object into the database
			$this->HUECO->ADD();
			// response OK. Also send post in content
			header($_SERVER['SERVER_PROTOCOL'].' 201 Created');
			header('Location: '.$_SERVER['REQUEST_URI']."/".$data->link);
			header('Content-Type: application/json');
			echo(json_encode(array(
				"link"=>$data->link,
				"fecha"=>$data->fecha,
				"hora_inicio" => $data->hora_inicio,
				"hora_fin" => $data->hora_fin
			)));

		} catch (ValidationException $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad request');
			header('Content-Type: application/json');
			echo(json_encode($e->getErrors()));
		}
	}

	public function borrarHueco($numero) {
		$currentUser = parent::authenticateUser()->email;
		
		if ( isset( $numero ) ) {
			$this->HUECO->numero=$numero;
			$this->HUECO->link = $this->HUECO->devolverLinkHueco();
		}

		if ($this->HUECO->link == NULL) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad request');
			echo("Hole with id ".$numero." not found");
			return;
		}
		// Check if the Post author is the currentUser (in Session)
		if ($this->HUECO->devolverPropietario() != $currentUser) {
			header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
			echo("you are not the author of this hole");
			return;
		}

		$this->HUECO->DELETE();

		header($_SERVER['SERVER_PROTOCOL'].' 204 No Content');
	}

}	

// URI-MAPPING for this Rest endpoint
$huecosRest = new HuecosRest();
URIDispatcher::getInstance()
->map("GET",	"/huecos/$1", array($huecosRest,"getHuecos"))
->map("DELETE", "/huecos/$1", array($huecosRest,"borrarHueco"))
->map("POST", "/huecos", array($huecosRest,"crearHueco"));
