<?php

require_once(__DIR__."/../Models/model_encuesta.php");
require_once(__DIR__."/BaseRest.php");
require_once(__DIR__."/../Exceptions/ValidationException.php");
/**
* Class PostRest
*
* It contains operations for creating, retrieving, updating, deleting and
* listing posts, as well as to create comments to posts.
*
* Methods gives responses following Restful standards. Methods of this class
* are intended to be mapped as callbacks using the URIDispatcher class.
*
*/
class EncuestasRest extends BaseRest {
	private $ENCUESTA;

	public function __construct() {
		parent::__construct();
		$this->ENCUESTA = new ENCUESTA( '', '', '' );
	}


	public function getEncuestas() {
		//recuperamos el email del obecto ModelUSER devuelto por el constructor
		$currentEmail = parent::authenticateUser()->email;
		$this->ENCUESTA->email=$currentEmail;

		//recuperamos las encuestas del usuario logeado
		$encuestas = $this->ENCUESTA->MIS_ENCUESTAS();

		// json_encode Post objects.
		// since Post objects have private fields, the PHP json_encode will not
		// encode them, so we will create an intermediate array using getters and
		// encode it finally
		$encuestas_array = array();

		foreach($encuestas as $datos) {
			array_push($encuestas_array, array(
				"link" => $datos['link'],
				"titulo" => $datos['titulo'],
				"email" => $datos['email'],
			));
		}

		header($_SERVER['SERVER_PROTOCOL'].' 200 Ok');
		header('Content-Type: application/json');
		echo(json_encode($encuestas_array));
	}

	public function actualizarEncuesta($link,$data){
		$currentEmail = parent::authenticateUser()->email;

		if ( isset($data->titulo) ) {
			$this->ENCUESTA->titulo = $data->titulo;
			$this->ENCUESTA->link = $link;
		}

		try {
			// validate Post object
			$this->ENCUESTA->checkIsValidForUpdate(); // if it fails, ValidationException

			// save the Post object into the database
			$this->ENCUESTA->EDIT();
			// response OK. Also send post in content
			header($_SERVER['SERVER_PROTOCOL'].' 201 Update');
			header('Location: '.$_SERVER['REQUEST_URI']."/".$data->link);
			header('Content-Type: application/json');
			echo(json_encode(array(
				"link"=>$data->link,
			)));

		} catch (ValidationException $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad request');
			header('Content-Type: application/json');
			echo(json_encode($e->getErrors()));
		}


		
	}

	public function crearEncuesta($data) {
		$currentEmail = parent::authenticateUser()->email;

		if (isset($data->link) && isset($data->titulo)) {
			$this->ENCUESTA->link = $data->link;
			$this->ENCUESTA->titulo = $data->titulo;
			$this->ENCUESTA->email = $currentEmail;
		}

		try {
			// validate Post object
			$this->ENCUESTA->checkIsValidForCreate(); // if it fails, ValidationException

			// save the Post object into the database
			$this->ENCUESTA->ADD();
			// response OK. Also send post in content
			header($_SERVER['SERVER_PROTOCOL'].' 201 Created');
			header('Location: '.$_SERVER['REQUEST_URI']."/".$data->link);
			header('Content-Type: application/json');
			echo(json_encode(array(
				"link"=>$data->link,
				"titulo"=>$data->titulo,
				"email" => $currentEmail
			)));

		} catch (ValidationException $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad request');
			header('Content-Type: application/json');
			echo(json_encode($e->getErrors()));
		}
	}
}
	
	
// URI-MAPPING for this Rest endpoint
$encuestasRest = new EncuestasRest();
URIDispatcher::getInstance()
->map("GET",	"/encuestas", array($encuestasRest,"getEncuestas"))
->map("POST", "/encuestas", array($encuestasRest,"crearEncuesta"))
->map("POST", "/encuestas/$1", array($encuestasRest,"actualizarEncuesta"));

