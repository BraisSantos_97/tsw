<?php
require_once(__DIR__."/../Models/model_usuario.php");
require_once(__DIR__."/BaseRest.php");
/**
* Class UserRest
*
* It contains operations for adding and check users credentials.
* Methods gives responses following Restful standards. Methods of this class
* are intended to be mapped as callbacks using the URIDispatcher class.
*
*/
class UserRest extends BaseRest {
	private $USUARIO;
	public function __construct() {
		parent::__construct();
		$this->USUARIO = new USUARIO( '', '', '', '' );
	}
	
	public function crearUsuario($data) {
		
		//$user = new USUARIO( $data->nombre, $data->apellidos, $data->email,$data->contrasena );
		if ( isset( $data->nombre) && isset($data->apellidos) && isset($data->email) && isset($data->contrasena) ) {
			$this->USUARIO->nombre = $data->nombre;
			$this->USUARIO->apellidos = $data->apellidos;
			$this->USUARIO->email = $data->email;
			$this->USUARIO->contrasena = $data->contrasena;
		}

		try {
			$this->USUARIO->checkIsValidForCreate();
			$this->USUARIO->ADD();
			header($_SERVER['SERVER_PROTOCOL'].' 201 Created');
			header("Location: ".$_SERVER['REQUEST_URI']."/".$data->email);
		}catch(ValidationException $e) {
			http_response_code(400);
			header('Content-Type: application/json');
			echo(json_encode($e->getErrors()));
		}
	}
		
	public function login($email) {
		$currentLogged = parent::authenticateUser()->email;
		if ($currentLogged != $email) {
			header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
			echo("You are not authorized to login as anyone but you");
		} else {
			header($_SERVER['SERVER_PROTOCOL'].' 200 Ok');
			echo("Hello ".$currentLogged);
		}
	}
}
// URI-MAPPING for this Rest endpoint
$userRest = new UserRest();
URIDispatcher::getInstance()
->map("GET",	"/usuario/$1", array($userRest,"login"))
->map("POST", "/usuario", array($userRest,"crearUsuario"));
