<?php
//pendiente de adaptacion
require_once(__DIR__."/../Models/model_marca.php");
require_once(__DIR__."/../Models/model_hueco.php");
require_once(__DIR__."/BaseRest.php");
require_once(__DIR__."/../Exceptions/ValidationException.php");
/**
* Class PostRest
*
* It contains operations for creating, retrieving, updating, deleting and
* listing posts, as well as to create comments to posts.
*
* Methods gives responses following Restful standards. Methods of this class
* are intended to be mapped as callbacks using the URIDispatcher class.
*
*/
class MarcasRest extends BaseRest {
	private $MARCA;

	public function __construct() {
		parent::__construct();
		$this->MARCA = new MARCA( '', '', '' );
	}

	public function getTotal($link) {
		//recuperamos el email del obecto ModelUSER devuelto por el constructor
		$currentEmail = parent::authenticateUser()->email;
		if ( isset( $link ) ) {
			$this->MARCA->link=$link;
			$this->MARCA->email=$currentEmail;
		}
		//huecos en una determinada encuesta
		$HUECO = new HUECO( '', '', '', $link, '');
		$huecos = $HUECO->RecuperarTodos();
		//usuarios que marcaron en una determinada encuesta
		$usuarios = $this->MARCA->RecuperarNombres($link);
		
		function encontrarMarca($marcas,$numeroHueco){
			if($marcas == NULL){
				return false;
			}
			foreach ($marcas as $marca) {
				if ($marca[1] == $numeroHueco) {
					return true;
				}
			}
			return false;
		}

		//Array que almacenara los datos
		$array_marcas = array();
		if($usuarios != null){
			foreach ($usuarios as $user) {
				foreach ($huecos as $hueco) {
					$this->MARCA->email=$user[0];
					$marcas = $this->MARCA->RecuperarMarcas();
					$array_marcas[$user[1]][] = encontrarMarca($marcas,$hueco[0]);
				}
			}
		}
		header($_SERVER['SERVER_PROTOCOL'].' 200 Ok');
		header('Content-Type: application/json');
		echo(json_encode($array_marcas));
	}

	public function getMarcas($link) {
		//recuperamos el email del obecto ModelUSER devuelto por el constructor
		$currentEmail = parent::authenticateUser()->email;
		if ( isset( $link ) ) {
			$this->MARCA->link=$link;
			$this->MARCA->email=$currentEmail;
		}

		//recuperamos las encuestas del usuario logeado
		
		$marcas = $this->MARCA->RecuperarAll();


		
		// json_encode Post objects.
		// since Post objects have private fields, the PHP json_encode will not
		// encode them, so we will create an intermediate array using getters and
		// encode it finally
		$marcas_array = array();

		foreach($marcas as $datos) {
			array_push($marcas_array, array(
				"email" => $datos[0],
				"numero" => $datos[1],
				"link" => $datos[2],
			));
		}

		header($_SERVER['SERVER_PROTOCOL'].' 200 Ok');
		header('Content-Type: application/json');
		echo(json_encode($marcas_array));
	}

	public function getMarcasUser($link,$userLogin) {
		//recuperamos el email del obecto ModelUSER devuelto por el constructor
		$currentEmail = parent::authenticateUser()->email;
		
		if ( isset( $link ) ) {
			$this->MARCA->link=$link;
			$this->MARCA->email=$userLogin;
		}

		//recuperamos las marcas de un usuario en concreto de una determinada encuesta
		$marcas = $this->MARCA->RecuperarMarcasUser();
		$HUECO = new HUECO( '', '', '', $link, '');
		$huecos = $HUECO->RecuperarTodos();
		
		$array_marcas_user = array();

		function encontrarMarca($marcas,$numeroHueco){
			if ($marcas == NULL) {
				return false;
			}
			foreach ($marcas as $marca) {
				if ($marca[0] == $numeroHueco) {
					return true;
				}
			}
			return false;
		}

		if ($huecos != NULL) {		
			foreach ($huecos as $hueco) {
				$array_marcas_user[$hueco[0]][] = encontrarMarca($marcas,$hueco[0]);
			}
		}

		header($_SERVER['SERVER_PROTOCOL'].' 200 Ok');
		header('Content-Type: application/json');
		echo(json_encode($array_marcas_user));
	}

	public function crearMarca($data) {
		$currentEmail = parent::authenticateUser()->email;

		if (isset($data->numero) && isset($data->link)) {
			$this->MARCA->email = $currentEmail;
			$this->MARCA->numero = $data->numero;
			$this->MARCA->link = $data->link;
		}

		try {
			// validate Post object
			$this->MARCA->checkIsValidForCreate(); // if it fails, ValidationException

			// save the Post object into the database
			$this->MARCA->ADD();
			// response OK. Also send post in content
			header($_SERVER['SERVER_PROTOCOL'].' 201 Created');
			header('Location: '.$_SERVER['REQUEST_URI']."/".$data->numero);
			header('Content-Type: application/json');
			echo(json_encode(array(
				"email"=>$currentEmail,
				"numero"=>$data->numero,
				"link" => $data->link
			)));

		} catch (ValidationException $e) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad request');
			header('Content-Type: application/json');
			echo(json_encode($e->getErrors()));
		}
	}
	
	//borrado de todas las marcas para insertar las nuevas marcas
	public function borrarMarcas($link) {
		$currentUser = parent::authenticateUser()->email;
		
		if ( isset( $link ) ) {
			$this->MARCA->link=$link;
			$this->MARCA->email = $currentUser;
		}

		if ($this->MARCA->link == NULL) {
			header($_SERVER['SERVER_PROTOCOL'].' 400 Bad request');
			echo("Marc with poll ".$link." not found");
			return;
		}

		die($this->MARCA->DELETE_ALL());

		header($_SERVER['SERVER_PROTOCOL'].' 204 No Content');
	}

}
	
	
// URI-MAPPING for this Rest endpoint
$marcasRest = new MarcasRest();
URIDispatcher::getInstance()
->map("GET",	"/marcas/$1", array($marcasRest,"getTotal"))
->map("GET",	"/encuestas/$1/marcas/$2", array($marcasRest,"getMarcasUser"))
->map("POST", "/marcas", array($marcasRest,"crearMarca"))
->map("DELETE", "/marcas/$1", array($marcasRest,"borrarMarcas"));


