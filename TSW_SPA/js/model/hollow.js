class HollowModel extends Fronty.Model {

  constructor(fecha, hora_inicio, hora_fin, numero, link) {
    super('HollowModel'); //call super
    
    if (fecha) {
      this.fecha = fecha;
    }
    
    if (hora_inicio) {
      this.hora_inicio = hora_inicio;
    }
    
    if (hora_fin) {
      this.hora_fin = hora_fin;
    }

    if (numero) {
      this.numero = numero;
    }

    if (link) {
      this.link = link;
    }
  }

  setFecha(fecha) {
    this.set((self) => {
      self.fecha = fecha;
    });
  }

  setHoraInicio(hora_inicio) {
    this.set((self) => {
      self.hora_inicio = hora_inicio;
    });
  }

  setHoraFin(hora_fin) {
    this.set((self) => {
      self.hora_fin = hora_fin;
    });
  }

  setNumero(numero) {
    this.set((self) => {
      self.numero = numero;
    });
  }

  setLink(link) {
    this.set((self) => {
      self.link = link;
    });
  }
  
}
