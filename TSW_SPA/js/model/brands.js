class BrandsModel extends Fronty.Model {

  constructor() {
    super('BrandsModel'); //call super

    // model attributes
    this.brands = [];
    this.brandsUser = [];
    this.hollows = [];
    this.email = '';
  }

  setSelectedBrand(brand) {
    this.set((self) => {
      self.selectedBrand = brand;
    });
  }

  setEmail(email) {
    this.set((self) => {
      self.email = email;
    });
  }

  setBrands(brands) {
    this.set((self) => {
      self.brands = brands;
    });
  }

  setBrandsUser(brandsUser) {
    this.set((self) => {
      self.brandsUser = brandsUser;
    });
  }

  setHollows(hollows) {
    this.set((self) => {
      self.hollows = hollows;
    });
  }

}
