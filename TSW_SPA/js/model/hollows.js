class HollowsModel extends Fronty.Model {

  constructor() {
    super('HollowsModel'); //call super

    // model attributes
    this.hollows = [];
    this.link = '';
    this.name = '';
    this.urlBase = '';
  }

  setSelectedHollow(hollow) {
    this.set((self) => {
      self.selectedHollow = hollow;
    });
  }

  setHollows(hollows) {
    this.set((self) => {
      self.hollows = hollows;
    });
  }

  setLinkHollows(link) {
    this.set((self) => {
      self.link = link;
    });
  }

  setNamePollHollows(name) {
    this.set((self) => {
      self.name = name;
    });
  }

  setUrlBasePollHollows(url) {
    this.set((self) => {
      self.urlBase = url;
    });
  }    

}
