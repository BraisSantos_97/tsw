class BrandModel extends Fronty.Model {

  constructor(email, numero, link) {
    super('BrandModel'); //call super
    
    if (email) {
      this.email = email;
    }
    
    if (numero) {
      this.numero = numero;
    }
    

    if (link) {
      this.link = link;
    }
  }

  setLink(link) {
    this.set((self) => {
      self.link = link;
    });
  }

  setTitulo(numero) {
    this.set((self) => {
      self.numero = numero;
    });
  }

  setEmail(email) {
    this.set((self) => {
      self.email = email;
    });
  }
  
}
