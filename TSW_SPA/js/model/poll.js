class PollModel extends Fronty.Model {

  constructor(link, titulo, email, urlBase) {
    super('PollModel'); //call super
    
    if (link) {
      this.link = link;
    }
    
    if (titulo) {
      this.titulo = titulo;
    }
    
    if (email) {
      this.email = email;
    }

    if (urlBase) {
      this.urlBase = urlBase;
    }
  }

  setLink(link) {
    this.set((self) => {
      self.link = link;
    });
  }

  setTitulo(titulo) {
    this.set((self) => {
      self.titulo = titulo;
    });
  }

  setEmail(email) {
    this.set((self) => {
      self.email = email;
    });
  }
  
}
