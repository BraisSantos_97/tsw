class HollowsService {
  constructor() {

  }

  //Encontrar encuestas del usuario
  findHollows(link) {
    return $.get(AppConfig.backendServer+'/rest/huecos/' + link);
  }

  deletePoll(numero) {
    return $.ajax({
      url: AppConfig.backendServer+'/rest/huecos/' + numero,
      method: 'DELETE'
    });
  }

  createPoll(hollow) {
    return $.ajax({
      url: AppConfig.backendServer+'/rest/encuestas/' + poll.link,
      method: 'POST',
      data: JSON.stringify(poll),
      contentType: 'application/json'
    });
  }

  createHollow(hollow) {
    return $.ajax({
      url: AppConfig.backendServer+'/rest/huecos',
      method: 'POST',
      data: JSON.stringify(hollow),
      contentType: 'application/json'
    });
  }

  //actualizar el nombre en el hollows component
  editPoll(poll) {
    return $.ajax({
      url: AppConfig.backendServer+'/rest/encuestas/' + poll.link,
      method: 'POST',
      data: JSON.stringify(poll),
      contentType: 'application/json'
    });
  }

}
