class BrandsService {
  constructor() {

  }

  //Encontrar encuestas del usuario
  findBrands(link) {
    return $.get(AppConfig.backendServer+'/rest/marcas/' + link);
  }
 	
  findHollows(link) {
    return $.get(AppConfig.backendServer+'/rest/huecos/' + link);
  }
  
  findBrandsUser(link,currentUser){
  	return $.get( AppConfig.backendServer + '/rest/encuestas/' + link + '/marcas/' + currentUser );
  }

  createBrand(brand){
  	return $.ajax({
      url: AppConfig.backendServer+'/rest/marcas',
      method: 'POST',
      data: JSON.stringify(brand),
      contentType: 'application/json'
    });
  }

  deleteBrands(link){
  	return $.ajax({
      url: AppConfig.backendServer+'/rest/marcas/' + link,
      method: 'DELETE'
    });
  }

}
