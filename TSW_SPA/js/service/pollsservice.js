class PollsService {
  constructor() {

  }

  //Encontrar encuestas del usuario
  findPolls() {
    return $.get(AppConfig.backendServer+'/rest/encuestas');
  }

  createPoll(poll) {
    return $.ajax({
      url: AppConfig.backendServer+'/rest/encuestas',
      method: 'POST',
      data: JSON.stringify(poll),
      contentType: 'application/json'
    });
  }

}
