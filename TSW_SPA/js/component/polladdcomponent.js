class PollsAddComponent extends Fronty.ModelComponent {
  constructor(pollsModel, userModel, pollModel, router) {
    super(Handlebars.templates.polladdtable, pollsModel, pollModel);
    
    
    this.pollsModel = pollsModel;
    this.userModel = userModel;
    this.pollModel = pollModel;
    this.addModel('user', userModel);
    this.router = router;

    this.pollsService = new PollsService();

    function generarLink(){
      var caracteres = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                    'O', 'P', 'Q', 'R', 'S', 'T', 'X', 'Y', 'Z','a', 'b', 'c', 'd', 'e',
                    'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                    't', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
      var numeroLetras = 6;
      var cadena = "";
      var i = 0;
      var rand = 0;

      for (i = 0; i < numeroLetras; i++) {
        rand = Math.floor(Math.random() * caracteres.length) + 1;
        cadena = cadena.concat(caracteres[rand % caracteres.length]);
      }

      return cadena;
    }

    this.addEventListener('click', '.boton_tabla', () => {
      var newPoll = {};
      newPoll.link = generarLink();
      newPoll.titulo = $('#titulo').val();
      newPoll.email = this.userModel.currentUser;
      this.pollsService.createPoll(newPoll)
        .then(() => {
          this.router.goToPage('view-hollows?link=' + newPoll.link + '&titulo=' + newPoll.titulo );
        })
        .fail((xhr, errorThrown, statusText) => {
          if (xhr.status == 400) {
            this.pollsModel.set(() => {
              this.pollsModel.pollAddErrors = xhr.responseJSON;
            });
          } else {
            alert( I18n.translate('an error has occurred during request: ') + statusText + '.' + xhr.responseText);
          }
        });
    });

  }

  

}
