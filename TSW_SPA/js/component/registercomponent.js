class RegisterComponent extends Fronty.ModelComponent {
  constructor(userModel, router) {
    super(Handlebars.templates.register, userModel);
    this.userModel = userModel;
    this.userService = new UserService();
    this.router = router;

    this.addEventListener('click', '#registerbutton', () => {
      this.userService.register({
          nombre: $('#registerusername').val(),
          apellidos: $('#registersurname').val(),
          email: $('#registeremail').val(),
          contrasena: $('#registerpassword').val()
        })
        .then(() => {
          alert(I18n.translate('User registered! Please login'));
          this.userModel.set((model) => {
            model.registerErrors = {};
            model.registerMode = false;
          });
          this.router.goToPage('login');
        })
        .fail((xhr, errorThrown, statusText) => {
          if (xhr.status == 400) {
            this.userModel.set(() => {
              this.userModel.registerErrors = xhr.responseJSON;
            });
          } else {
            alert( I18n.translate('an error has occurred during request: ') + statusText + '.' + xhr.responseText);
          }
        });
    });
  }
}
