class MainComponent extends Fronty.RouterComponent {
  constructor() {
    super('frontyapp', Handlebars.templates.main, 'maincontent');

    // models instantiation
    // we can instantiate models at any place
    var userModel = new UserModel();
    var pollsModel = new PollsModel();
    var hollowsModel = new HollowsModel();
    var brandsModel = new BrandsModel();
    var pollModel = new PollModel();

    super.setRouterConfig({
      login: {
        component: new LoginComponent(userModel, this),
        title: 'Login'
      },
      register: {
        component: new RegisterComponent(userModel, this),
        title: 'Register'
      },
      polls: {
        component: new PollsComponent(pollsModel, userModel, this),
        title: 'Polls'
      },
      pollAdd: {
        component: new PollsAddComponent(pollsModel, userModel, pollModel ,this),
        title: 'Poll Add'
      },
      'view-hollows': {
        component: new HollowsComponent(hollowsModel, userModel, this),
        title: 'Hollows'
      },
      'hollowAdd': {
        component: new HollowAddComponent(hollowsModel, userModel, this),
        title: 'Add Hollow'
      },
      'view-brands': {
        component: new BrandsComponent(brandsModel, userModel, this),
        title: 'Brands'
      },
      defaultRoute: 'login'
    });
    
    Handlebars.registerHelper('currentPage', () => {
          return super.getCurrentPage();
    });

    var userService = new UserService();
    this.addChildComponent(this._createUserBarComponent(userModel, userService));
    this.addChildComponent(this._createLanguageComponent());
    this.addChildComponent(this._createMenuComponent(userModel));

  }

  _createUserBarComponent( userModel, userService ) {
    var userbar = new Fronty.ModelComponent(Handlebars.templates.user, userModel, 'navegacion');

    userbar.addEventListener('click', '#logoutbutton', () => {
      userModel.logout();
      userService.logout();
      this.goToPage('login');
    });

    // do relogin
    userService.loginWithSessionData()
      .then(function(logged) {
        if (logged != null) {
          userModel.setLoggeduser(logged);
        }
      });

    return userbar;
  }

  _createLanguageComponent() {
    var languageComponent = new Fronty.ModelComponent(Handlebars.templates.language, this.routerModel, 'languagecontrol');
    // language change links
    languageComponent.addEventListener('click', '#englishlink', () => {
      I18n.changeLanguage('default');
      document.location.reload();
    });

    languageComponent.addEventListener('click', '#spanishlink', () => {
      I18n.changeLanguage('es');
      document.location.reload();
    });

    return languageComponent;
  }

  _createMenuComponent(userModel) {
    var menuComponent = new Fronty.ModelComponent(Handlebars.templates.menu, userModel, 'lateral');

    return menuComponent;
  }

}
