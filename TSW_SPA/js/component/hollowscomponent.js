class HollowsComponent extends AuthenticatedComponent {
  constructor(hollowsModel, userModel, router) {
    super(Handlebars.templates.hollowstable, hollowsModel, userModel, null, null);
    
    
    this.hollowsModel = hollowsModel;
    this.userModel = userModel;
    this.addModel('user', userModel);
    this.router = router;

    this.hollowsService = new HollowsService();

    this.addEventListener('click', '.addImage', (event) => {
      var link = event.target.getAttribute('item');
      var titulo = event.target.getAttribute('item2');
      this.router.goToPage( 'hollowAdd?link=' + link + '&titulo=' + titulo );
    });


    function newNamePoll(oldtitle){
      var name = prompt(I18n.translate('Please enter new title:'), oldtitle);
      var txt = '';

      if (name == null) {
        return; //break out of the function early
      }

      if (name.trim() == null || name.trim() == "") {
        txt = I18n.translate('The new name can not be empty');
      } else if( !/^([A-Za-zá-úÁ-Ú]+\s*)+$/.test( name.trim() ) ){
        txt = I18n.translate('must be formed only by alphabetic characters');
      } else {
        txt = I18n.translate('The name has been changed');
      }

      alert(txt);

      return name;
    }

    this.addEventListener('click', '.editImage', (event) => {
        var link = event.target.getAttribute('item');
        //var oldtitle = event.target.getAttribute('item2');
        var oldtitle = this.router.getRouteQueryParam('titulo');

        var title = newNamePoll(oldtitle);

        var updatePoll = {};
        updatePoll.titulo = title;
        updatePoll.link = link;

        this.hollowsService.editPoll(updatePoll)
          .fail(() => {
            alert(I18n.translate( 'poll cannot be actualized') ),
            title = oldtitle;
          })
          .always(() => {
            this.updateHollows(link,title);
          });
    });

  }

  authenticatedOnStart() {
    var link = this.router.getRouteQueryParam('link');
    var titulo = this.router.getRouteQueryParam('titulo');
    this.updateHollows(link,titulo);
  }

  updateHollows(link,titulo) {
    this.hollowsModel.setNamePollHollows(titulo);
    this.hollowsModel.setLinkHollows(link);
    this.hollowsModel.setUrlBasePollHollows(window.location.origin + window.location.pathname);

    this.hollowsService.findHollows(link).then((data) => {
      this.hollowsModel.setHollows(
        // create a Fronty.Model for each item retrieved from the backend
        data.map(
          (item) => new HollowModel(item.fecha, item.hora_inicio, item.hora_fin, item.numero, item.link)
      ));
    });

  }

  // Override
  createChildModelComponent(className, element, id, modelItem) {
    return new HollowRowComponent(modelItem, this.userModel, this.router, this);
  }
}

class HollowRowComponent extends Fronty.ModelComponent {
  constructor(hollowModel, userModel, router, hollowsComponent) {
    super(Handlebars.templates.hollowrow, hollowModel, null, null);
    
    this.hollowsComponent = hollowsComponent;
    
    this.userModel = userModel;
    this.addModel('user', userModel); // a secondary model
    
    this.router = router;

    this.addEventListener('click', '.delete', (event) => {
      if (confirm(I18n.translate('Are you sure?'))) {
        var numero = event.target.getAttribute('item');
        this.hollowsComponent.hollowsService.deletePoll(numero)
          .fail(() => {
            alert('poll cannot be deleted')
          })
          .always(() => {
            this.hollowsComponent.authenticatedOnStart();
          });
      }
    });

    this.addEventListener('click', '.edit-button', (event) => {
      var postId = event.target.getAttribute('item');
      this.router.goToPage('edit-post?id=' + postId);
    });
  }

}
