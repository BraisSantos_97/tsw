class AuthenticatedComponent extends Fronty.ModelComponent {
	constructor(template, model, userModel, nodeId, childTags) {
		super(template, model, nodeId, childTags);
		this.userModel = userModel;
	}

	onStart() {
		if (!this.userModel.currentUser) {
			console.log('user is not logged yet, we will wait...')
			this.userModel.addObserver(() => {
				console.log('user has been logged in');
				if (this.userModel.currentUser) {
					this.authenticatedOnStart();
				}
			});
		} else {
			console.log('user is authenticated, we can start normally...');
			this.authenticatedOnStart();
		}
	}

	/*authenticatedOnStart() {
		// to be overriden
	}*/
}