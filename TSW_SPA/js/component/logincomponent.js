class LoginComponent extends Fronty.ModelComponent {
  constructor(userModel, router) {
    super(Handlebars.templates.login, userModel);
    this.userModel = userModel;
    this.userService = new UserService();
    this.router = router;

    this.addEventListener('click', '#loginbutton', (event) => {
      this.userService.login($('#login').val(), $('#password').val())
        .then(() => {
          this.router.goToPage('polls');
          this.userModel.setLoggeduser($('#login').val());
        })
        .catch((error) => {
          this.userModel.set((model) => {
            model.loginError =  error.responseText.trim();
          });
          this.userModel.logout();
        });
    });

    this.addEventListener('click', '#registerlink', () => {
      this.userModel.set(() => {
        this.userModel.registerMode = true;
        this.router.goToPage('register');
      });
    });

    
  }
}
