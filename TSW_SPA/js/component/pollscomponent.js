class PollsComponent extends AuthenticatedComponent {
  constructor(pollsModel, userModel, router) {
    super(Handlebars.templates.pollstable, pollsModel, userModel, null, null);
    
    
    this.pollsModel = pollsModel;
    this.userModel = userModel;
    this.addModel('user', userModel);
    this.router = router;

    this.pollsService = new PollsService();

  }

  authenticatedOnStart() {
    this.updatePolls();
  }

  updatePolls() {
    var urlBase = window.location.origin + window.location.pathname ;

    this.pollsService.findPolls().then((data) => {
      this.pollsModel.setPolls(
        // create a Fronty.Model for each item retrieved from the backend
        data.map(
          (item) => new PollModel(item.link, item.titulo, item.email, urlBase)
      ));
    });
  }

  // Override
  createChildModelComponent(className, element, id, modelItem) {
    return new PollRowComponent(modelItem, this.userModel, this.router, this);
  }
}

class PollRowComponent extends Fronty.ModelComponent {
  constructor(pollModel, userModel, router, pollsComponent) {
    super(Handlebars.templates.pollrow, pollModel, null, null);
    
    this.pollsComponent = pollsComponent;
    
    this.userModel = userModel;
    this.addModel('user', userModel); // a secondary model
    
    this.router = router;

    this.addEventListener('click', '.remove-button', (event) => {
      if (confirm(I18n.translate('Are you sure?'))) {
        var postId = event.target.getAttribute('item');
        this.postsComponent.postsService.deletePost(postId)
          .fail(() => {
            alert( I18n.translate('poll cannot be deleted') )
          })
          .always(() => {
            this.postsComponent.updatePosts();
          });
      }
    });

    this.addEventListener('click', '.iconoTabla', (event) => {
      var link = event.target.getAttribute('item');
      var titulo = event.target.getAttribute('item2');
      this.router.goToPage('view-hollows?link=' + link + '&titulo=' + titulo );
    });

    this.addEventListener('click', '.iconoTabla', (event) => {
      var link = event.target.getAttribute('item');
      var titulo = event.target.getAttribute('item2');
      this.router.goToPage('view-brands?link=' + link );
    });

  }

}
