class BrandsComponent extends AuthenticatedComponent {
  constructor(brandsModel, userModel, router) {
    super(Handlebars.templates.brandstable, brandsModel, userModel, null, null);
    
    
    this.brandsModel = brandsModel;
    
    this.userModel = userModel;
    this.addModel('user', userModel);

    this.router = router;

    this.brandsService = new BrandsService();


    this.addEventListener('click', '.boton_tabla', () => {
      var link = this.router.getRouteQueryParam('link');
      var email = userModel.currentUser;
      //borramos los datos para guardar lo marcado
      this.brandsService.deleteBrands(link).then((data) => {
      
      });

      $("input[type=checkbox]:checked").each(function(){
        var brandsService = new BrandsService();
        var newBrand = {};
        newBrand.email = email;
        newBrand.link = link;
        newBrand.numero = $(this).val();
        brandsService.createBrand(newBrand).then((data) => {

        });
      });
      alert( I18n.translate('Poll collected') );
      this.router.goToPage('view-brands?link=' + link);
      this.onStart();
    });

  }

  authenticatedOnStart() {
    var link = this.router.getRouteQueryParam('link');
    var currentUser = this.userModel.currentUser;
    this.brandsModel.setEmail(currentUser);
    this.updateBrands(link);
    this.updateHollows(link);
    this.updateBrandsUser(link,currentUser);
  }

  updateBrands(link) {
    this.brandsService.findBrands(link).then((data) => {
      this.brandsModel.setBrands(data);
      });
  }

  updateBrandsUser(link,currentUser) {
    this.brandsService.findBrandsUser(link,currentUser).then((data) => {
      this.brandsModel.setBrandsUser(data);
      });
  }

  updateHollows(link) {
    this.brandsService.findHollows(link).then((data) => {
      this.brandsModel.setHollows(
        // create a Fronty.Model for each item retrieved from the backend
        data.map(
          (item) => new HollowModel(item.fecha, item.hora_inicio, item.hora_fin, item.numero, item.link)
      ));
    });
  }

  // Override
  createChildModelComponent(className, element, id, modelItem) {
    return new BrandRowComponent(modelItem, this.userModel, this.router, this);
  }
}

class BrandRowComponent extends Fronty.ModelComponent {
  constructor(brandModel, userModel, router, brandsComponent) {
    super(Handlebars.templates.brandrow, brandModel, null, null);
    
    this.brandsComponent = brandsComponent;
    
    this.userModel = userModel;
    this.addModel('user', userModel); // a secondary model
    
    this.router = router;

    this.addEventListener('click', '.remove-button', (event) => {
      if (confirm(I18n.translate('Are you sure?'))) {
        var postId = event.target.getAttribute('item');
        this.postsComponent.postsService.deletePost(postId)
          .fail(() => {
            alert( I18n.translate('post cannot be deleted') )
          })
          .always(() => {
            this.postsComponent.updatePosts();
          });
      }
    });

    this.addEventListener('click', '.iconoTabla', (event) => {
      var link = event.target.getAttribute('item');
      var titulo = event.target.getAttribute('item2');
      this.router.goToPage('view-hollows?link=' + link + '&titulo=' + titulo );
    });
  }

}
