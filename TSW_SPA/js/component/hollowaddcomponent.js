class HollowAddComponent extends Fronty.ModelComponent {
  constructor(hollowsModel, userModel, router) {
    super(Handlebars.templates.hollowaddtable, hollowsModel, null, null);
    
    
    this.hollowsModel = hollowsModel;
    this.userModel = userModel;
    this.addModel('user', userModel);
    this.router = router;

    this.hollowsService = new HollowsService();

    this.addEventListener('click', '.boton_tabla', () => {
      var link = this.router.getRouteQueryParam('link');
      var titulo = this.router.getRouteQueryParam('titulo');
      var newHollow = {};
      newHollow.link = link;
      newHollow.fecha = $('#fecha').val();
      newHollow.hora_inicio = $('#hora_inicio').val();
      newHollow.hora_fin = $('#hora_fin').val();
      this.hollowsService.createHollow(newHollow)
        .then(() => {
          this.router.goToPage('view-hollows?link=' + link + '&titulo=' + titulo );
        })
        .fail((xhr, errorThrown, statusText) => {
          if (xhr.status == 400) {
            this.hollowsModel.set(() => {
              this.hollowsModel.errors = xhr.responseJSON;
            });
          } else {
            alert( I18n.translate( 'an error has occurred during request: ') + statusText + '.' + xhr.responseText);
          }
        });
    });

  }

  afterRender() {
    f_tcalInit();
  }
  

}
