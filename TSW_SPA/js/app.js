/* Main mvcblog-front script */

//load external resources
function loadTextFile(url) {
  return new Promise((resolve, reject) => {
    $.get({
      url: url,
      cache: true,
      dataType: 'text'
    }).then((source) => {
      resolve(source);
    }).fail(() => reject());
  });
}


// Configuration
var AppConfig = {
  backendServer: 'http://localhost/TSW_SPA'
  //backendServer: '/mvcblog'
}

Handlebars.templates = {};
Promise.all([
    I18n.initializeCurrentLanguage('js/i18n'),
    loadTextFile('templates/components/main.hbs').then((source) =>
      Handlebars.templates.main = Handlebars.compile(source)),
    loadTextFile('templates/components/language.hbs').then((source) =>
      Handlebars.templates.language = Handlebars.compile(source)),
    loadTextFile('templates/components/menu.hbs').then((source) =>
      Handlebars.templates.menu = Handlebars.compile(source)),    
    loadTextFile('templates/components/user.hbs').then((source) =>
      Handlebars.templates.user = Handlebars.compile(source)),
    loadTextFile('templates/components/login.hbs').then((source) =>
      Handlebars.templates.login = Handlebars.compile(source)),
     loadTextFile('templates/components/register.hbs').then((source) =>
      Handlebars.templates.register = Handlebars.compile(source)),
    loadTextFile('templates/components/polls-table.hbs').then((source) =>
      Handlebars.templates.pollstable = Handlebars.compile(source)),
    loadTextFile('templates/components/poll-row.hbs').then((source) =>
      Handlebars.templates.pollrow = Handlebars.compile(source)),
    loadTextFile('templates/components/hollow-row.hbs').then((source) =>
      Handlebars.templates.hollowrow = Handlebars.compile(source)),
    loadTextFile('templates/components/hollows-table.hbs').then((source) =>
      Handlebars.templates.hollowstable = Handlebars.compile(source)),
    loadTextFile('templates/components/poll-add-table.hbs').then((source) =>
      Handlebars.templates.polladdtable = Handlebars.compile(source)),
    loadTextFile('templates/components/hollow-add-table.hbs').then((source) =>
      Handlebars.templates.hollowaddtable = Handlebars.compile(source)),
    loadTextFile('templates/components/brands-table.hbs').then((source) =>
      Handlebars.templates.brandstable = Handlebars.compile(source))
  ])
  .then(() => {
    $(() => {
      new MainComponent().start();
    });
  }).catch((err) => {
    alert('FATAL: could not start app ' + err);
  });
