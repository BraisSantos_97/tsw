<?php

class HUECO {

	var $fecha;
	var $hora_inicio;
	var $hora_fin;
    var $numero;
    var $link;

function __construct($fecha,$hora_inicio,$hora_fin,$link,$numero){
	
    $this->fecha = $fecha;
	$this->hora_inicio = $hora_inicio;
	$this->hora_fin = $hora_fin;
    $this->link=$link;
    $this->numero = $numero;
   
    
   

	// incluimos la funcion de acceso a la bd
	require_once(__DIR__."/../Functions/BdAdmin.php");
	// conectamos con la bd y guardamos el manejador en un atributo de la clase
	$this->mysqli = ConectarBD();

}

public function checkIsValidForCreate() {
	$errors = array();
	if (strlen(trim($this->link)) == 0 ) {
		$errors["link"] = "title is mandatory";
	}
	if (strlen(trim($this->fecha)) == 0 ) {
		$errors["fecha"] = "fecha is mandatory";
	}
	if (strlen(trim($this->hora_inicio)) == 0 ) {
		$errors["hora_inicio"] = "hora inicio is mandatory";
	}
	if (strlen(trim($this->hora_fin)) == 0 ) {
		$errors["hora_fin"] = "hora_fin is mandatory";
	}
	
	if (sizeof($errors) > 0){
		throw new ValidationException($errors, "encuesta is not valid");
	}
}

function devolverLinkHueco(){
	$sql = "SELECT link FROM huecos
						WHERE numero = '$this->numero'";
	$resultado = $this->mysqli->query( $sql );
	if ( $resultado->num_rows == 0 ) 
    { return null; }
	
	while($datos = mysqli_fetch_row ($resultado)){
		$miarray[] = $datos;
	}
	return $miarray[0][0];	
}

function devolverPropietario(){
	$sql = "SELECT e.email FROM encuesta e,huecos h 
						   WHERE e.link = h.link AND
						   h.numero = '$this->numero' AND
						   h.link = '$this->link'";
	$resultado = $this->mysqli->query( $sql );
	if ( $resultado->num_rows == 0 ) 
    { return null; }
	
	while($datos = mysqli_fetch_row ($resultado)){
		$miarray[] = $datos;
	}
	return $miarray[0][0];	
}

function ADD()
{
   
    
    if (($this->link <> '' )){ 
		
		$sql = "SELECT * FROM huecos WHERE (link = '$this->link' AND numero= '$this->numero')";
        
		if (!$result = $this->mysqli->query($sql)){ 
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ 
				// Variable que almacena la insercción de un nuevo usuario
				$sql = "INSERT INTO huecos (
					fecha,
                    hora_inicio,
                    hora_fin,
					link
					) 
						VALUES (
						'$this->fecha',
						'$this->hora_inicio',
						'$this->hora_fin',
                        '$this->link'
						)";
               
                
				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción'; // Error en la inserción
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}
				
			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
        return 'Introduzca un valor'; // introduzca un valor para el usuario
	}
} 
    function DELETE()
        {	// se construye la sentencia sql de busqueda con los atributos de la clase
            $sql = "SELECT * FROM huecos WHERE (link = '$this->link' AND numero = '$this->numero')";
            // se ejecuta la query
            $result = $this->mysqli->query($sql);
            // si existe una tupla con ese valor de clave
            if ($result->num_rows == 1)
            {
                // se construye la sentencia sql de borrado
                $sql = "DELETE FROM huecos WHERE (link = '$this->link' AND numero = '$this->numero')";
                // se ejecuta la query
                $this->mysqli->query($sql);
                // se devuelve el mensaje de borrado correcto

                return true;
            } // si no existe el login a borrar se devuelve el mensaje de que no existe
            else
                return false;
        } // fin metodo DELETE


	function MIS_HUECOS($email){

		$sql = "SELECT fecha,hora_inicio,hora_fin,numero,e.link 
					FROM encuesta e, huecos h 
						where
						 e.link=h.link and 
						 e.email='$email' and
						 e.link='$this->link'
						ORDER BY fecha,hora_inicio ";
       
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { 
			return $resultado;
		}
	}

	function TODOS_LOS_HUECOS(){

		$sql = "SELECT fecha,hora_inicio,hora_fin,numero,e.link 
					FROM encuesta e, huecos h 
						where
						 e.link=h.link and 
						 e.link='$this->link'
						ORDER BY fecha,hora_inicio ";
       
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { 
			return $resultado;
		}
	}

	function RellenaDatos() { 

		$sql = "SELECT e.titulo,e.link,h.fecha,h.hora_inicio,h.hora_fin,h.numero,h.link
				FROM encuesta e,huecos h 
				WHERE (
			       	  e.link = h.link &&
				      e.link = '$this->link' 
					  )
				ORDER BY fecha,hora_inicio";
		
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; 
		} else { 
			return $resultado;
		}
	}

	function RecuperarTodos() { 

		$sql = "SELECT h.numero
				FROM encuesta e,huecos h 
				WHERE (
			       	  e.link = h.link &&
				      e.link = '$this->link' 
					  )
				ORDER BY fecha,hora_inicio";
		
		$resultado = $this->mysqli->query( $sql );
		if ( $resultado->num_rows == 0 ) 
        { return null; }
		
		while($datos = mysqli_fetch_row ($resultado)){
			$miarray[] = $datos;
		}
		return $miarray;		
	}
    
    function rellenar()
        {	// se construye la sentencia de busqueda de la tupla
            $sql = "SELECT * FROM huecos WHERE (link = '$this->link' AND numero = '$this->numero')";
            // Si la busqueda no da resultados, se devuelve el mensaje de que no existe

            $resultado = $this->mysqli->query($sql);
          
            if (!($resultado)){
                return 'No existe en la base de datos'; 
            }
            else{ // si existe se devuelve la tupla resultado
                $result = $resultado->fetch_array();
                return $result;
  
            }
        
        } // fin del metodo RellenaDatos()

	function __destruct()
	{

	}

}

?> 