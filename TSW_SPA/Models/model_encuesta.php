<?php

class ENCUESTA {

	var $link;
	var $titulo;
	var $email;

function __construct($link,$titulo,$email){
	$this->link = $link;
	$this->titulo = $titulo;
	$this->email = $email;
		
	// incluimos la funcion de acceso a la bd
	require_once(__DIR__."/../Functions/BdAdmin.php");
	//include_once 'Functions/BdAdmin.php';
	// conectamos con la bd y guardamos el manejador en un atributo de la clase
	$this->mysqli = ConectarBD();

}


public function checkIsValidForCreate() {
	$errors = array();
	if (strlen(trim($this->link)) == 0 ) {
		$errors["link"] = "link is mandatory";
	}

	if (strlen(trim($this->titulo)) == 0 ) {
		$errors["titulo"] = "title is mandatory";
	} else if ( !preg_match("/^([A-Za-zá-úÁ-Ú]+\s*)+$/",$this->titulo) ){
		$errors["titulo"] = "must be formed only by alphabetic characters";
	}

	if ($this->email == NULL ) {
		$errors["email"] = "email is mandatory";
	}
	if (sizeof($errors) > 0){
		throw new ValidationException($errors, "encuesta is not valid");
	}
}

public function checkIsValidForUpdate() {
	$errors = array();
	
	if (strlen(trim($this->titulo)) == 0 ) {
		$errors["titulo"] = "content is mandatory";
	}else if ( !preg_match("/^([A-Za-zá-úÁ-Ú]+\s*)+$/",$this->titulo) ){
		$errors["titulo"] = "must be formed only by alphabetic characters";
	}

	if (sizeof($errors) > 0){
		throw new ValidationException($errors, "encuesta is not valid");
	}
}

function ADD()
{
    if (($this->link <> '')){ 
		
		$sql = "SELECT * FROM encuesta WHERE (link = '$this->link')";
        
		if (!$result = $this->mysqli->query($sql)){ 
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ 
				// Variable que almacena la insercción de un nuevo usuario
				$sql = "INSERT INTO encuesta (
					link,
					titulo,
					email
					) 
						VALUES (
						'$this->link',
						'$this->titulo',
						'$this->email'
						)";
				
				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción'; // Error en la inserción
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}
				
			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
        return 'Introduzca un valor'; // introduzca un valor para el usuario
	}
} 


function EDIT() {
		// se construye la sentencia de busqueda de la tupla en la bd
		$sql = "SELECT * FROM encuesta WHERE (link = '$this->link')";
		// se ejecuta la query
		$result = $this->mysqli->query( $sql );
		// si el numero de filas es igual a uno es que lo encuentra
		if ( $result->num_rows == 1 ) {
			// se construye la sentencia de modificacion en base a los atributos de la clase

			//modificamos los atributos de la tabla USUARIO
			$sql = "UPDATE encuesta 
					SET	titulo = '$this->titulo'
                    WHERE ( link = '$this->link' )";
			// si hay un problema con la query se envia un mensaje de error en la modificacion
			if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { // si no hay problemas con la modificación se indica que se ha modificado
				return 'Modificado correctamente';
			}

		} // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
		else {
			return 'No existe en la base de datos';
		}
	} // fin del metodo EDIT

	function MIS_ENCUESTAS(){
	//SELECT DISTINCT e.titulo FROM encuesta e,huecos h,marca m WHERE (e.link = h.link && h.link = m.link && m.email = 'email') || e.email = 'email';
    //select DISTINCT e.titulo,e.link,e.email, h.*, m.* from encuesta e LEFT OUTER JOIN huecos h ON h.link = e.link LEFT OUTER JOIN marca m ON m.link = h.link AND m.numero = h.numero where ( e.email = 'tsw' )

		$sql = "select DISTINCT e.titulo,e.link,e.email
                from encuesta e LEFT OUTER JOIN huecos h ON h.link=e.link LEFT OUTER JOIN marca m ON m.link=h.link AND m.numero=h.numero
    			where 
    				(
    					(e.link = h.link &&
    					h.link = m.link &&
    					m.email ='$this->email') ||
    					e.email = '$this->email'
    				)";
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { 
			return $resultado;
		}
	}

	function devolverPropietario(){
		$sql = "SELECT email FROM encuesta WHERE link = '$this->link'";
		$resultado = $this->mysqli->query( $sql );
		if ( $resultado->num_rows == 0 ) 
        { return null; }
		
		while($datos = mysqli_fetch_row ($resultado)){
			$miarray[] = $datos;
		}
		return $miarray[0][0];	
	}

	function __destruct()
	{

	}

}

?> 