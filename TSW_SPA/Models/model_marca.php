<?php

class MARCA {

	var $email;
	var $numero;
    var $link;

function __construct($email,$numero,$link){
	
    $this->email = $email;
	$this->numero = $numero;
    $this->link=$link;
		
	// incluimos la funcion de acceso a la bd
	require_once(__DIR__."/../Functions/BdAdmin.php");
	// conectamos con la bd y guardamos el manejador en un atributo de la clase
	$this->mysqli = ConectarBD();

}


public function checkIsValidForCreate() {
	$errors = array();
	if (strlen(trim($this->link)) == 0 ) {
		$errors["link"] = "link is mandatory";
	}
	if (strlen(trim($this->numero)) == 0 ) {
		$errors["numero"] = "numero is mandatory";
	}
	if ($this->email == NULL ) {
		$errors["email"] = "email is mandatory";
	}
	if (sizeof($errors) > 0){
		throw new ValidationException($errors, "marca is not valid");
	}
}



function ADD()
{
    if (($this->link <> '' && $this->numero <> '' && $this->email <> '')){ 
		
		$sql = "SELECT * FROM marca WHERE (link = '$this->link' && numero = '$this->numero' && email = '$this->email')";
        
		if (!$result = $this->mysqli->query($sql)){ 
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ 
				// Variable que almacena la insercción de un nuevo usuario
				$sql = "INSERT INTO marca (
					email,
                    numero,
                    link
					) 
						VALUES (
						'$this->email',
						'$this->numero',
                        '$this->link'
						)";
				
				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción'; // Error en la inserción
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}
				
			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
        return 'Introduzca un valor'; // introduzca un valor para el usuario
	}
} 
    function DELETE()
        {	// se construye la sentencia sql de busqueda con los atributos de la clase
            $sql = "SELECT * FROM marca WHERE (link = '$this->link' AND numero = '$this->numero')";
            // se ejecuta la query
            $result = $this->mysqli->query($sql);
            // si existe una tupla con ese valor de clave
            if ($result->num_rows == 1)
            {
                // se construye la sentencia sql de borrado
                $sql = "DELETE FROM marca WHERE (link = '$this->link' AND numero = '$this->numero')";
                // se ejecuta la query
                $this->mysqli->query($sql);
                // se devuelve el mensaje de borrado correcto

                return "Borrado correctamente";
            } // si no existe el login a borrar se devuelve el mensaje de que no existe
            else
                return "No existe";
        } // fin metodo DELETE


    function EDIT() {
		$sql = "SELECT * FROM marca WHERE (numero = '$this->numero' && link = '$this->link')";

		$result = $this->mysqli->query( $sql );
		
		if ( $result->num_rows == 1 ) { 
			
				$sql = "UPDATE ACCION SET 
					IdAccion = '$this->IdAccion',
					 NombreAccion='$this->NombreAccion',
                     DescripAccion='$this->DescripAccion'
				WHERE ( IdAccion  = '$this->IdAccion'
				)"; 
            
		
			if ( !( $result = $this->mysqli->query( $sql ) ) ) {
				return 'Error en la modificación';
			} else { 
				return 'Modificado correctamente';
			}

		} else 
			return 'No existe en la base de datos';
	} 

	function DELETE_ALL()
        {	// se construye la sentencia sql de busqueda con los atributos de la clase
            $sql = "SELECT * FROM marca WHERE (link = '$this->link' && email = '$this->email')";
            
            $result = $this->mysqli->query($sql);
            
            if ($result->num_rows >= 1)
            {
                // se construye la sentencia sql de borrado
                $sql = "DELETE FROM marca WHERE (link = '$this->link' && email = '$this->email')";
                // se ejecuta la query
                $this->mysqli->query($sql);
                // se devuelve el mensaje de borrado correcto

                return "Borrado correctamente";
            } // si no existe el login a borrar se devuelve el mensaje de que no existe
            else
                return "No existe";
        } // fin metodo DELETE

	function MIS_HUECOS($link){
	//SELECT DISTINCT e.titulo FROM encuesta e,huecos h,marca m WHERE (e.link = h.link && h.link = m.link && m.email = 'email') || e.email = 'email';
        $email = $_SESSION['email'];
		$sql = "SELECT fecha,hora_inicio,hora_fin FROM encuesta e, huecos h where e.link=h.link and e.email='$email' and e.link='$link'";
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'Error en la consulta sobre la base de datos';
		} else { 
			return $resultado;
		}
	}

	//recupera las marcas del usuario logeado en una encuesta en concreto
	function RecuperarMarcas() { 

		$sql = "SELECT link,numero,email
				FROM marca 
				WHERE (
			       	  link = '$this->link' &&
			       	  email = '$this->email'
					  )";
		
		$resultado = $this->mysqli->query( $sql );
		if ( $resultado->num_rows == 0 ) 
        { return null; }
		
		while($datos = mysqli_fetch_row ($resultado)){
			$miarray[] = $datos;
		}
		return $miarray;		
	}

	function RecuperarAll() { 

		$sql = "SELECT email,numero,link
				FROM marca 
				WHERE (
			       	  link = '$this->link' &&
			       	  email != '$this->email'
					  )";
		
		$resultado = $this->mysqli->query( $sql );
		if ( $resultado->num_rows == 0 ) 
        { return null; }
		
		while($datos = mysqli_fetch_row ($resultado)){
			$miarray[] = $datos;
		}
		return $miarray;
				
	}  
	
	function RecuperarNombres($link) { 

		$sql = "SELECT DISTINCT u.email,u.nombre
				FROM usuario u,marca m
				WHERE( 
					u.email = m.email &&
					m.link = '$link' &&
					m.email != '$this->email'
					)";
		
		$resultado = $this->mysqli->query( $sql );
		
		if ( $resultado->num_rows == 0 ) 
        { return null; }
		
		while($datos = mysqli_fetch_row ($resultado)){
			$miarray[] = $datos;
		}
		return $miarray;

				
	}

	function RecuperarMarcasUser(){
		$sql = "SELECT numero FROM marca 
							  WHERE ( email = '$this->email' AND
							  		link = '$this->link' )";

		$resultado = $this->mysqli->query( $sql );
		
		if ( $resultado->num_rows == 0 ) 
        { return null; }
		
		while($datos = mysqli_fetch_row ($resultado)){
			$miarray[] = $datos;
		}
		return $miarray;
	}


	function __destruct()
	{

	}

}

?> 
