<?php

class USUARIO {

	var $nombre;
	var $apellidos;
	var $email;
	var $contrasena;

function __construct($nombre,$apellidos,$email,$contrasena){
	$this->nombre = $nombre;
	$this->apellidos = $apellidos;
	$this->email = $email;
	$this->contrasena = $contrasena;
		
	// incluimos la funcion de acceso a la bd
	require_once(__DIR__."/../Functions/BdAdmin.php");
	// conectamos con la bd y guardamos el manejador en un atributo de la clase
	$this->mysqli = ConectarBD();

}

function getEmail(){
	return $this->email;
}

public function checkIsValidForCreate() {
		$patronCorreo="/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/";

		$errors = array();
		
		if (strlen(trim($this->nombre)) == 0 ) {
			$errors["nombre"] = "name is mandatory";
		} else if ( !preg_match("/^([A-Za-zá-úÁ-Ú]+\s*)+$/",$this->nombre) ){
			$errors["nombre"] = "must be formed only by alphabetic characters";
		}


		if (strlen(trim($this->apellidos)) == 0 ) {
			$errors["apellidos"] = "surname is mandatory";
		} else if ( !preg_match("/^([A-Za-zá-úÁ-Ú]+\s*)+$/",$this->apellidos) ){
			$errors["apellidos"] = "must be formed only by alphabetic characters";
		}

		if (strlen(trim($this->email)) == 0 ) {
			$errors["email"] = "email is mandatory";
		} else if (!preg_match($patronCorreo,$this->email)) {
			$errors["email"] = "does not meet the format of an email";
		}
		
		if (strlen(trim($this->contrasena)) == 0 ) {
			$errors["contrasena"] = "password is mandatory";
		}

		if( !$this->Register() ){
			$errors["email"] = "email existent in database";
		}
		
		if (sizeof($errors) > 0){
			throw new ValidationException($errors, "user is not valid");
		}
	}

function ADD()
{
    if (($this->email <> '')){ 
		
		// construimos el sql para buscar esa clave en la tabla
		// Variable que almacena el contenido de la consulta
        $sql = "SELECT * FROM usuario WHERE (email = '$this->email')";
        // si da error la 
		//construimos la sentencia sql de inserción en la bdejecución de la query
		if (!$result = $this->mysqli->query($sql)){ 
			return 'No se ha podido conectar con la base de datos'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ 
				// Variable que almacena la insercción de un nuevo usuario
				$sql = "INSERT INTO usuario (
					nombre,
					email,
					password,
					apellidos
					) 
						VALUES (
						'$this->nombre',
						'$this->email',
						'$this->contrasena',
						'$this->apellidos'
						)";
				
				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					return 'Error en la inserción'; // Error en la inserción
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					return 'Inserción realizada con éxito'; //operacion de insertado correcta
				}
				
			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'Ya existe en la base de datos'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
        return 'Introduzca un valor'; // introduzca un valor para el usuario
	}
} 

function __destruct()
{

}

function login(){
	//Variable que almacena el contenido de la consulta de busqueda del login introducido en la base de datos
	$sql = "SELECT *
			FROM usuario
			WHERE (
				(email = '$this->email') 
			)";
	//Variable que almacena el contenido del resultado de la consulta
	$resultado = $this->mysqli->query($sql);
	//Si el numero de columnas devueltas es igual a 0 envia el mensaje de que no existe ese usuario
	if ($resultado->num_rows == 0){
		return false;
	}
	//Si el numero de columnas es distinto de 0 comparamos la contraseña del login para comprobar si es correcta
	//si no es correcta envia un mensaje de que la contraseña no es correcta para este usuario.
	else{
		//Varable que almacena la tupla recogida en la base de datos
		$tupla = $resultado->fetch_array();
		//Si la password es igual que la password introducida devuelve "true"
		if ($tupla['password'] == $this->contrasena){
			return true;
		}
		//Si no es correcta devuelve un mensaje de que la contraseña no es correcta para este usuario.
		else{
			return false;
		}
	}
}

function Register(){
		//Variable que almacena la consulta del email en la base de datos
		$sql = "select * from usuario where email = '".$this->email."'";
		//Varaible que almacena el resultado de la consulta
		$result = $this->mysqli->query($sql);
		// si el numero de columnas es igual a 1, muestra el mensaje de que el usuario existe
		if ($result->num_rows == 1){
				return false;
			}
		// En caso contrario devuelve true significando que no existe usuario coincidente
		else{
	    		return true;
		}

	}


	function RecuperaDatos(){
	//Variable que almacena el contenido de la consulta de busqueda del login introducido en la base de datos
	$sql = "SELECT *
			FROM usuario
			WHERE (
				(email = '$this->email') 
			)";
	//Variable que almacena el contenido del resultado de la consulta
		if ( !( $resultado = $this->mysqli->query( $sql ) ) ) {
			return 'No existe en la base de datos'; 
		} else { 
			return $resultado->fetch_array();
		}

	}

	

}

?> 