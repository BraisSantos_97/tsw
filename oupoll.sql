-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-10-2018 a las 01:22:32
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `OuPoll`
--

CREATE DATABASE IF NOT EXISTS `oupoll` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `oupoll`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

CREATE TABLE `encuesta` (
  `link` varchar(200) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`link`, `titulo`, `email`) VALUES
('lbHvgt', 'quedada', 'dani@gmail.com'),
('S4v0M2', 'cumple brais', 'dani@gmail.com'),
('TDS5Id', 'halloween', 'brais@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `huecos`
--

CREATE TABLE `huecos` (
  `fecha` varchar(20) NOT NULL,
  `hora_inicio` varchar(10) NOT NULL,
  `hora_fin` varchar(10) NOT NULL,
  `numero` int(11) NOT NULL,
  `link` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `huecos`
--

INSERT INTO `huecos` (`fecha`, `hora_inicio`, `hora_fin`, `numero`, `link`) VALUES
('10/10/2018', '22:15', '23:00', 1, 'lbHvgt'),
('24/10/2018', '21:15', '21:45', 2, 'lbHvgt'),
('24/10/2018', '23:00', '03:45', 3, 'S4v0M2'),
('31/10/2018', '00:00', '01:45', 4, 'S4v0M2'),
('09/10/2018', '01:45', '08:45', 5, 'TDS5Id');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `email` varchar(30) NOT NULL,
  `numero` int(11) NOT NULL,
  `link` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`email`, `numero`, `link`) VALUES
('brais@gmail.com', 2, 'lbHvgt'),
('brais@gmail.com', 3, 'S4v0M2'),
('brais@gmail.com', 5, 'TDS5Id'),
('dani@gmail.com', 1, 'lbHvgt'),
('dani@gmail.com', 4, 'S4v0M2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `email` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellidos` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`email`, `password`, `nombre`, `apellidos`) VALUES
('brais@gmail.com', 'brais', 'Brais', 'Santos Negreira'),
('dani@gmail.com', 'dani', 'Dani', 'Gonzalez Peña');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD PRIMARY KEY (`link`),
  ADD KEY `FK_USUARIO` (`email`);

--
-- Indices de la tabla `huecos`
--
ALTER TABLE `huecos`
  ADD PRIMARY KEY (`numero`,`link`),
  ADD KEY `FK_ENCUESTA` (`link`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`email`,`numero`,`link`),
  ADD KEY `FK_HUECOS` (`numero`,`link`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `huecos`
--
ALTER TABLE `huecos`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `encuesta`
--
ALTER TABLE `encuesta`
  ADD CONSTRAINT `FK_USUARIO` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `huecos`
--
ALTER TABLE `huecos`
  ADD CONSTRAINT `FK_ENCUESTA` FOREIGN KEY (`link`) REFERENCES `encuesta` (`link`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `marca`
--
ALTER TABLE `marca`
  ADD CONSTRAINT `FK_HUECOS` FOREIGN KEY (`numero`,`link`) REFERENCES `huecos` (`numero`, `link`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_USU` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;


--
-- Usuario para que realice las operaciones con la base de datos
--

grant all privileges on oupoll.* to tsw@localhost identified by "tsw";

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
